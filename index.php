<?php

$domain = "http://opeka-website.local";
require_once 'vendor/autoload.php';
$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader, array('cache' => 'compilation_cache', 'auto_reload' => true));

$navigation = array(
	array('href' => 'products', 'caption' => 'Продукты', 'tmpl' => 'products.html' ),
	array('href' => 'develop', 'caption' => 'В разработке', 'tmpl' => 'develop.html'),
	array('href' => 'api', 'caption' => 'API', 'tmpl' => 'api.html'),
	array('href' => 'smi', 'caption' => 'СМИ о нас', 'tmpl' => 'smi.html'),
	array('href' => 'contacts', 'caption' => 'Контакты', 'tmpl' => 'contacts.html'),
	array('href' => 'partners', 'caption' => 'Дилеры', 'tmpl' => 'dealers.html'),
	array('href' => 'op4', 'caption' => 'Подробнее об Опека-4', 'tmpl' => 'opeka4.html'),
);

$opekas = array(
	array('href' => 'op1', 'caption' => 'Подробнее об Опека-1', 'tmpl' => 'opeka1.html' ),
	array('href' => 'op2', 'caption' => 'Подробнее об Опека-2', 'tmpl' => 'opeka2.html'),
	array('href' => 'op3', 'caption' => 'Подробнее об Опека-3', 'tmpl' => 'opeka3.html'),
	array('href' => 'op4', 'caption' => 'Подробнее об Опека-4', 'tmpl' => 'opeka4.html'),
);

$a = basename($_SERVER['REQUEST_URI']);

switch ($a) {
	case "":
	case "index.php":
		echo $twig->render('index.html', array('navigation' => $navigation));
		break;
	default:
		for ($x=0; $x<=count($navigation); $x++) {
			if (isset($navigation[$x]['href'])) {
				if ($a == $navigation[$x]['href']) {
				echo $twig->render($navigation[$x]['tmpl'], array('navigation' => $navigation, 'links' => $opekas));
				}
			}
		}
		break;
	}

for ($y=0; $y<=count($opekas); $y++) {
	if (isset($opekas[$y]['href'])) {
		if ($a == $opekas[$y]['href']) {
			echo $twig->render($opekas[$y]['tmpl'], array('navigation' => $navigation));
			}
		}
	}


?>
